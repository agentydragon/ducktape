server {
    listen 80;
    server_name agentydragon.com www.agentydragon.com;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    server_name agentydragon.com www.agentydragon.com;

    ssl_certificate /etc/letsencrypt/live/agentydragon.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/agentydragon.com/privkey.pem;

    root /var/www/agentydragon.com;
    index index.html index.htm;

    # TODO: what's this for?
    location / {
        try_files $uri $uri/ =404;
    }
}
