---
title: Congratulations for scanning my NFC tag!
---


Hi!

<!-- TODO: float right -->
<figure>
<img src="/static/2022-sticker-hello.png" style="max-width: 6cm">
</figure>

Given that you're here, you probably scanned my NFC armband at a con or something.
Yay!

If you're a furry, you probably want my Telegram - [@agentydragon][telegram] :)

Other contacts:
<!-- TODO: unify with about.markdown? -->

* email: `agentydragon@gmail.com`
* Matrix: [@agentydragon:matrix.org][matrix]
* Mastodon: [@agentydragon@mastodon.social][mastodon]

And there's some more in the [About page][about].

[telegram]: https://telegram.me/agentydragon
[matrix]: https://matrix.to/#/@agentydragon:matrix.org
[mastodon]: https://mastodon.social/web/@agentydragon
[about]: /about.html
