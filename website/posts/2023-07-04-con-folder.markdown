---
title: "Furry life-hack: Per-con/per-event messenger folders"
---

This is just to spread a small life-hack I came up with for furry conventions.
I'll probably post a couple more of those small life-hacks next couple days, if
things go well. (Coming back from [Anthrocon][anthrocon] with some sorta
con-crud so I'm a little dead. Probably [kissed too many boys][boy-kisser].)

When you're at a con (or an event of any sort), in your messenger app (mine is
Telegram), make a folder with everyone you know at that event, or any event
chats you wanna follow.

This is how that looked for me at Anthrocon:

<img src="/static/2023-07-04-telegram-folders.png" alt="Telegram screenshot of
the configation">

Then, when you're in between things and are looking for people to join/stuff to
do, just go through that folder.

Having all the people in one folder makes it way easier to remember "oh I wanted
to meet person X", or to just shower everyone in a "hi I'm free wanna hang out"
message.

(And in Telegram, you can also put that folder to the top of the chat group
list, which makes sense if you're at a con - then you probably want to mostly
chat with other people at the con anyway.)

[anthrocon]: https://www.anthrocon.org/
[boy-kisser]: https://knowyourmeme.com/memes/oooooo-you-like-boys-ur-a-boykisser
