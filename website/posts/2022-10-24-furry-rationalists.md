---
title: Furry Rationalists & Effective Anthropomorphism both exist
---

(cross-posted on [LessWrong][lw] and [EA Forum][eaf])

Hi!

<figure>
<img src="/static/2022-sticker-hello.png" style="max-width: 8cm">
</figure>

I’m Rai and I’m a furry (specifically, dragon). The last couple years, I’ve been
running a Furry Rationalists Telegram group. It looks like we exist, and not
everyone who should know we exist does yet, so I wanted to just write this to
advertise that this furry+EA/rationality corner exists, and if you’re
furry-adjacent & rationality-adjacent and nice, you’re invited to join us :)

Here’s the invite link for the Furry Rationalists group:
<https://t.me/+KTmU37JO3Wk0OTQ0>

There’s ~50 of us and we’re chill - we have self-improvement, science, and cute
animal GIFs. If you’d like a preview, here’s the [guidelines + meta doc][guidelines].
We’re 18+, but we’re not adult-oriented - we’re 18+ just so that we can talk
about adult stuff if it does come up. If you happen to be <18 and wanna join,
let me know, we might update this.

If you’re reading this a while later, and the link expired, contact me (via
some method on this website), or look us up on
<https://www.furry-telegram-groups.net/>, a search for “rationality” should find
us.

There’s also a smaller Effective Anthropomorphism Discord server, run by
[bird][bird]: <https://discord.gg/JBeKs5qZ>

Come say hi, and feel free to share if you know anyone who’d be interested!

[lw]: https://www.lesswrong.com/posts/kEchp4aP9pdvdRLRf/furry-rationalists-and-effective-anthropomorphism-both-exist
[eaf]: https://forum.effectivealtruism.org/posts/Sj9RMx3gn8oxi3ycX/furry-rationalists-and-effective-anthropomorphism-both-exist
[guidelines]: https://docs.google.com/document/d/1xqNgRYzdVFO736oOIHK2oTIN3F8gty-lyYMYFyz1dLQ/view
[bird]: https://twitter.com/birdpathy
