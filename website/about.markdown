---
title: About
---

<!-- md5("agentydragon@gmail.com") -->
<img src="http://www.gravatar.com/avatar/a389909145981708ab2c9fc8bddbe92c?s=200"
     class="avatar"
     style="float: right;"
     alt="Me">

Hi!

My chosen name is Rai, and my given name is Michal Pokorný. I'm 30, I grew up
in Czech Republic. I've worked as a software engineer at Google Zurich since
March 2016, then took a year-long break roughly May 2021 - May 2022. Then I
joined OpenAI, first as a resident on the reinforcement learning team, then on
alignment.

I have a bachelor's in general CS from the [Faculty of Mathematics and Physics][mff]
of the Charles University in Prague. I started a master's in AI at the very same,
and passed all parts but the "you have to write a thesis" one.
<!-- TODO: stredni skola? -->

I am an [effective altruist][ea] ([Czech][ea-cz]) and I currently aim to give
~10%.

I hunted a few geocaches:
<a href="https://www.geocaching.com/profile/?guid=8011109d-8913-415c-9272-3e82426f3835" target="_blank">
<img src="https://img.geocaching.com/stats/img.aspx?txt=Ooooh,+shiny!&uid=8011109d-8913-415c-9272-3e82426f3835&bg=1" alt="Geocaching badge" />
</a>

I try to be a generalist. Some stuff I do or did before:

 * Lots of Google stack in C++, Python. Services, gigabyte-terabyte scale
   pipelines, stuff like that.
 * Web stuff. In the distant past, I used PHP, then Ruby (on Rails and off),
   and I mostly use Python today. Of course, there's JavaScript and its
   offshoots.
 * I sometimes write software that needs a lot of performance.
   I mostly use C or C++ for that, but I also like Go.
 * I also wrote a few desktop things in Java and C#. I'm a bit more competent
   in the latter.

Between 2012 and 2016, I helped organize [KSP][ksp] -- a computer science
seminar for high school students by Charles University.

## Socials

### Preferred

<!-- TODO: unify with found.markdown? -->

* Telegram: [@agentydragon][telegram]. Trying to migrate off it to Matrix.
* Mastodon: [@agentydragon@mastodon.social][mastodon]
* Matrix: [@agentydragon:matrix.org][matrix]
* email: `agentydragon@gmail.com`
* GitLab: [agentydragon][gitlab]

### Other

* BookWyrm: [agentydragon@bookwyrm.social][bookwyrm]. It's got my book reviews,
  reading list etc.
* GitHub: [agentydragon][github]. Trying to migrate off it because I am not
  happy with GitHub, owned by Microsoft, being a critical dependency of
  many open-source projects.
* LinkedIn: [agentydragon][linkedin]. Please do not send connection requests if
  we don't yet know each other from somewhere.
* Facebook: [agentydragon][facebook]. I am not a fan of Facebook, though, and
  basically only use it for finding events. If you want to chat with me, prefer
  Matrix or Telegram. I lean towards only having Facebook friends that I
  actually know in person.

[gitlab]: https://gitlab.com/agentydragon
[matrix]: https://matrix.to/#/@agentydragon:matrix.org
[bookwyrm]: https://bookwyrm.social/user/agentydragon
[mastodon]: https://mastodon.social/web/@agentydragon
[ea]: https://www.effectivealtruism.org/
[ea-cz]: http://www.efektivni-altruismus.cz/
[mff]: http://www.mff.cuni.cz/
[github]: https://github.com/agentydragon
[linkedin]: https://linkedin.com/in/agentydragon
[facebook]: https://facebook.com/agentydragon
[ksp]: https://ksp.mff.cuni.cz/
[telegram]: https://telegram.me/agentydragon
