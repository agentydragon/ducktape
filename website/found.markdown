---
title: Found my stuff?
---

Hi! If you're here, you probably scanned a QR code / entered this URL from a
sticker on a thing of mine. And either you're just curious, or my thing is lost.

If it's the latter, I'd appreciate if you help the missing thing find its way
back to me :)

To do a good deed, please poke me on one of those:

<!-- TODO: unify with about.markdown? -->

* email: `agentydragon@gmail.com`
* Telegram: [@agentydragon][telegram]
* Matrix: [@agentydragon:matrix.org][matrix]
* Mastodon: [@agentydragon@mastodon.social][mastodon]

Otherwise, at least do something cool with the thing :)

[telegram]: https://telegram.me/agentydragon
[matrix]: https://matrix.to/#/@agentydragon:matrix.org
[mastodon]: https://mastodon.social/web/@agentydragon
