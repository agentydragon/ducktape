Dotfiles
========

This directory contains my dotfiles.
I use [rcm](https://github.com/thoughtbot/rcm) to manage them. Their
installation is managed by Ansible.

To list rc: `lsrc`

To add new RC files: `mkrc ~/.tigrc`

`rcup -B agentydragon`
