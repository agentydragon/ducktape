# Ducktape

My personal infrastructure's duct tape. Projects that didn't yet warrant making
into separate repositories.

Based on my [Python project skeleton](https://gitlab.com/agentydragon/python-skeleton).

## License
AGPL 3.0

## Updates

To update Python requirements lock:

```bash
bazel run //:requirements.update
```
